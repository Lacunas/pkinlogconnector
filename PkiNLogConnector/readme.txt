﻿--------------------------------------------------------------------------------
                            Lacuna PKI NLog Connector
--------------------------------------------------------------------------------

This package enables sending log messages generated by the Lacuna PKI SDK to the
NLog logging library.

NOTE: In order to maximise compatibility, this package depends on an old version
of the NLog package, but we STRONGLY RECOMMEND that you install the latest
version (currently the latest version with confirmed compatibility is 4.0.1, but
any future 4.x versions should also work).

For more information regarding the Lacuna PKI SDK, please visit:

	http://pki.lacunasoftware.com/

Usage
-----

All you need to do is make the following call somewhere in your code, preferably
on the startup of your application or website:

	Lacuna.Pki.NLogConnector.NLogLogger.Configure();

From then on, all log messages generated by the SDK will be forwarded to NLog.
Configuration of NLog, however, is up to you. The following would be an example
of a web.config/app.config file to save log messages to a file:

	<?xml version="1.0" encoding="utf-8"?>
	<configuration>
		<configSections>
			<section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
		</configSections>
		<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<targets async="true">
				<target name="pkiLog" xsi:type="File" fileName="C:\Logs\LacunaPKI.log" layout="${longdate} ${level}: ${message}" />
			</targets>
			<rules>
				<logger name="Lacuna.Pki.*" writeTo="pkiLog" />
			</rules>
		</nlog>
	</configuration>

NOTE: It is very important to specify asynchronous logging (async="true" on the
targets tag), otherwise the performance of the SDK might be considerably
impaired.

The messages are sent with various logger names according to where the message
was originated, but all logger names are preceded by "Lacuna.Pki." (hence the
name="Lacuna.Pki.*" attribute on the logger tag).

Source code
-----------

This package is open source. Feel free to fork it if you need to make any
customizations:

	https://bitbucket.org/Lacunas/pkinlogconnector

