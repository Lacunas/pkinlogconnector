﻿using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lacuna.Pki.NLogConnector {

	public class NLogLogger : ILogger {

		public static void Configure() {
			PkiConfig.Logger = new NLogLogger();
		}

		private ConcurrentDictionary<string, Logger> loggers;

		public NLogLogger() {
			loggers = new ConcurrentDictionary<string, Logger>();
		}

		public void Log(LogLevels level, string message, string source) {

			var logger = loggers.GetOrAdd(source, s => LogManager.GetLogger(s));

			LogLevel logLevel;

			switch (level) {
				case LogLevels.Trace:
					logLevel = LogLevel.Trace;
					break;
				case LogLevels.Info:
					logLevel = LogLevel.Info;
					break;
				case LogLevels.Warning:
					logLevel = LogLevel.Warn;
					break;
				case LogLevels.Error:
					logLevel = LogLevel.Error;
					break;
				default:
					logLevel = LogLevel.Info; // should not happen, but we'll not fail to log the entry should this happen
					break;
			}

			logger.Log(logLevel, message);
		}

		public void Flush() {
			NLog.LogManager.Flush();
		}
	}
}
