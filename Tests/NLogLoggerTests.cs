﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NLog.Config;
using NLog.Targets;
using System.IO;
using NLog;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading.Tasks;
using NLog.Targets.Wrappers;
using System.Diagnostics;

namespace Lacuna.Pki.NLogConnector.Tests {

	[TestClass]
	public class NLogLoggerTests {
	
		[TestMethod]
		public void SingleThreadTest() {

			var iterations = 1000;
			var tempFile = configureNLog();

			NLogLogger.Configure();
			var sw = Stopwatch.StartNew();
			for (int i = 0; i < iterations; i++) {
				PkiConfig.Logger.Log(LogLevels.Info, "Message #" + i, "Lacuna.Pki.xxx");
			}
			sw.Stop();
			PkiUtil.FlushLog();

			// Execution of the loop should be nearly instantaneous, since we've specified asynchronous logging (excluding the call to Finalize, which flushes the log) 
			Assert.IsTrue(sw.Elapsed < TimeSpan.FromMilliseconds(100));

			var indexes = new List<int>();
			var regex = new Regex(@"Message #(\d+)");
			foreach (var line in File.ReadAllLines(tempFile)) {
				var match = regex.Match(line);
				if (match.Success) {
					indexes.Add(int.Parse(match.Groups[1].Value));
				}
			}

			Assert.AreEqual(iterations, indexes.Count);
			Enumerable.Range(0, iterations).ToList().ForEach(i => Assert.IsTrue(indexes.Contains(i)));

			tryDeleteFile(tempFile);
		}

		[TestMethod]
		public void MultiThreadTest() {

			var iterations = 1000;
			var tempFile = configureNLog();

			NLogLogger.Configure();
			var sw = Stopwatch.StartNew();
			Parallel.For(0, iterations, i => PkiConfig.Logger.Log(LogLevels.Info, "Message #" + i, "Lacuna.Pki.xxx"));
			sw.Stop();
			PkiUtil.FlushLog();

			// Execution of the loop should be nearly instantaneous, since we've specified asynchronous logging (excluding the call to Finalize, which flushes the log) 
			Assert.IsTrue(sw.Elapsed < TimeSpan.FromMilliseconds(100));

			var indexes = new List<int>();
			var regex = new Regex(@"Message #(\d+)");
			foreach (var line in File.ReadAllLines(tempFile)) {
				var match = regex.Match(line);
				if (match.Success) {
					indexes.Add(int.Parse(match.Groups[1].Value));
				}
			}

			Assert.AreEqual(iterations, indexes.Count);
			Enumerable.Range(0, iterations).ToList().ForEach(i => Assert.IsTrue(indexes.Contains(i)));

			tryDeleteFile(tempFile);
		}

		#region Helper methods

		private static string configureNLog() {		
			var tempFile = Path.GetTempFileName();		
			var config = new LoggingConfiguration();			
			var fileTarget = new FileTarget();
			fileTarget.FileName = tempFile;
			fileTarget.Layout = "${longdate} ${level}: ${message}";
			var asyncWrapperTarget = new AsyncTargetWrapper(fileTarget);
			config.AddTarget("pkiLog", asyncWrapperTarget);
			config.LoggingRules.Add(new LoggingRule("Lacuna.Pki.*", LogLevel.Trace, asyncWrapperTarget));
			LogManager.Configuration = config;
			return tempFile;
		}

		private static void tryDeleteFile(string tempFile) {
			try {
				File.Delete(tempFile);
			} catch {
			}
		}

		#endregion
	}
}
