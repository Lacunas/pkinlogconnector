﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lacuna.Pki.NLogConnector.TestsWithLatestDependencies {
	
	[TestClass]
	public class NLogLoggerTests : Lacuna.Pki.NLogConnector.Tests.NLogLoggerTests {

		[TestMethod]
		public void SingleThreadTest_Latest() {
			base.SingleThreadTest();
		}

		[TestMethod]
		public void MultiThreadTest_Latest() {
			base.MultiThreadTest();
		}

	}
}
